#!make
include .env

all: prep start

prep: verify setup

verify:
ifeq (,$(wildcard .env))
	$(error Global environment configuration missing, please create it based on the provided template/)
endif
ifeq (,$(wildcard conf/openssl.conf))
	$(error OpenSSL configuration missing, please create it based on the template inside conf/)
endif

setup:
	@echo '>> Creating project structure'
	printf 'WEBSOCKET_ENABLED=true\n' > conf/vaultwarden/.env
	printf "DOMAIN=https://${HOST_NAME}\n" >> conf/vaultwarden/.env
	mkdir -p data/vaultwarden/
	
	@echo
	@echo '>> Generating TLS certs'
	./gen_certs.sh
	
	@echo
	@echo '>> Done setting up. Start the environment via'
	@echo '    docker-compose up -d'
	@echo

start:
	docker-compose up -d

stop:
	docker-compose down

restart: stop start

logs:
	docker-compose logs -f
