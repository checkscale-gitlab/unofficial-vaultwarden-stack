# Unofficial Vaultwarden Stack

The intention of this project is to provide a simple way of setting up your own network hosted password manager in Docker while keeping the necessary amount of manual configuration to a minimum.



## Overview

This stack consists of 3 individual components:

- [Vaultwarden](https://github.com/dani-garcia/vaultwarden) — “An alternative implementation of the Bitwarden server API written in Rust and compatible with upstream Bitwarden clients.”
- [Traefik Proxy](https://traefik.io/traefik/) — “[A] modern reverse proxy and load balancer […]”
- [OpenSSL](https://www.openssl.org/) — “[A] toolkit for the [TLS] and [SSL] protocols.”

Vaultwarden is the actual password manager service used by this stack. It is compatible with all – at least all *official* – Bitwarden clients. This means that you can access the password vault either via the browser or through the Bitwarden desktop and mobile clients as well as browser extensions.

Traefik Proxy is used to allow for DNS and IP based access to Vaultwarden, to supply the TLS certificates for the respective HTTPS endpoints, and to enforce TLS access by rerouting HTTP requests to HTTPS.

OpenSSL is not necessary for the regular operation of the stack. The toolkit will only be invoked during the initial setup for generating the self-signed certificate to be used for TLS.

All of these components are provided via Docker images so no tools other than the Docker runtime need to be installed locally to be able to run this stack.



## Minimal Configuration

There are currently two things that need to be configured before being able to run this stack:

- `.env` — Global config
- `conf/openssl.conf` — OpenSSL config

References and documentation for these is located in the respective `.template`, `.example` and `.reference` files.



## Automatic Deployment

Simply run `make` in the root of this project to generate the self-signed certificate and start up Docker Compose with the necessary services automatically.

After `make` has finished executing it usually takes about a minute for the services to start up. By default you will then find the Vaultwarden service running on port 80/443 and the Traefik dashboard on port 8080.

If you do not wish to start the services immediately run `make prep` instead. This will only set up the necessary Vaultwarden configuration and generate the self-signed certificate.

**Important:** Do not run `make` to re-launch the service after e.g. a reboot since this would also regenerate the self-signed certificate. Simply use `make start` or `docker-compose up -d` instead.



## Manual Deployment

If you do not have `make` installed on your machine or want to run the deployment manually you can follow the following steps to create the Docker Compose stack instead:

1. Create a `.env` file in `conf/vaultwarden/` with the following contents:

   ```properties
   WEBSOCKET_ENABLED=true
   DOMAIN=https://<YOUR_HOSTNAME>
   ```

   This is a minimum configuration for Vaultwarden. To see additional options check out the `.env.template` file in the same directory.

2. Create the directories `data/vaultwarden/`. This is where the data for Vaultwarden will be stored.

3. Run the `gen_certs.sh` script in the root of this project to generate the self-signed certificate.

   Alternatively, if you want to supply your own certificate, add it as `pwdmgr.crt` and `pwdmgr.key` files for the public and private keys respectively to the `certs/` directory.

4. Run `docker-compose up -d` to start Docker Compose in detached mode.



## Make Command Summary

| Command      | Description                                                  |
| ------------ | ------------------------------------------------------------ |
| make         | Verify, prepare and launch the environment                   |
| make prep    | Verify and prepare the environment                           |
| make verify  | Verify if mandatory config files are in place                |
| make setup   | Run environment preparation:<br />- Set up required Vaultwarden configuration<br />- Generate self-signed certificate |
| make start   | Launch the password manager stack                            |
| make stop    | Stop the password manager stack                              |
| make restart | Restart the password manager stack                           |
| make logs    | Inspect Docker Compose logs                                  |



## TODO

- [ ] Add optional support for letsencrypt
- [ ] Add optional support for plain HTTP (discouraged)

